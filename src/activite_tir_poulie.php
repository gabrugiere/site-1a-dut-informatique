<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8">
		<title>Activité arc poulie | Association Robin des bois</title>
		<link rel="icon" type="image/jpg" href="images/favicon.jpg" />
		<link rel="stylesheet" href="style/general.css" type="text/css" media="screen" />
	</head>
	<body>
		<header>
				<?php include 'include/header.php'; ?>
		</header>

		<main>
			<div class="social">
				<?php include 'include/social.php'; ?>
			</div>
				
			<div class="aside">
				<?php include 'include/aside.php'; ?>
			</div>
				
			<div class="principal">
				<section>
					<h2>Le tir à l'arc à poulie</h2>
					<p><hr /></p>
					<article>
						<p>La deuxième activité proposée est celle du tir à l'arc à poulie, arc plus puissant que l'arc classique grâce à deux poulies se trouvant à chaque bout de l'arc. Les séances ressemblent à celles du tir à l'arc classique, avec différentes distances et tailles des cibles, ainsi que des séances en extérieur. Seul l'arc change au final.</p>
					</article>
				</section>
			</div>
		</main>
		
		<footer>
				<?php include 'include/footer.php'; ?>
		</footer>
	</body>
</html>
