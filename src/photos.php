<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8">
		<title>Nos photos | Association Robin des bois</title>
		<link rel="icon" type="image/jpg" href="images/favicon.jpg" />
		<link rel="stylesheet" href="style/general.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="style/photos.css" type="text/css" media="screen" />
	</head>
	<body>
		<header>
				<?php include 'include/header.php'; ?>
		</header>

		<main>
			<div class="social">
				<?php include 'include/social.php'; ?>
			</div>
				
			<div class="aside">
				<?php include 'include/aside.php'; ?>
			</div>
			
			<div class="principal">
				<section>
					<h2>Nos photos</h2>
					<p><hr /></p>
					<article>
						<h3>Arc classique</h3>
						<a href="seance.php" title="Une séance classique"><img src="photos/arc_classique/seance.jpg" alt="Une séance classique" title="Une séance classique"/></a>
						<a href="materiel_tir_classique.php" title="Du matériel de tir"><img src="photos/arc_classique/materiel.jpg" alt="Du matériel de tir" title="Du matériel de tir"/></a>
						<a href="" title="Zoom sur un arc avant de le tendre"><img src="photos/arc_classique/arc.jpg" alt="Zoom sur un arc avant de le tendre" title="Zoom sur un arc avant de le tendre"/></a>
						<a href="" title="Participation de l'association à une compétition"><img src="photos/arc_classique/competition1.jpg" alt="Participation de l'association à une compétition" title="Participation de l'association à une compétition"/></a>
						<a href="" title="Marie Agnes, championne de l'équipe"><img src="photos/arc_classique/marie-agnes-competition.JPG" alt="Marie Agnes, championne de l'équipe" title="Marie Agnes, championne de l'équipe"/></a>
					</article>
					<article>
						<h3>Arc à poulie</h3>
						<a href="" title="Un arc à poulie"><img src="photos/arc_poulie/arc-poulie.jpg" alt="Un arc à poulie" title="Un arc à poulie"/></a>
						<a href="" title="Une séance classique de tir à l'arc à poulie"><img src="photos/arc_poulie/séance-classique.jpg" alt="Une séance classique de tir à l'arc à poulie" title="Une séance classique de tir à l'arc à poulie"/></a>
						<a href="" title="Participation de l'association aux championnats de France"><img src="photos/arc_poulie/championnat-de-france.jpg" alt="Participation de l'association aux championnats de France" title="Participation de l'association aux championnats de France"/></a>
						<a href="" title="Nos champions"><img src="photos/arc_poulie/champions.jpg" alt="Nos champions" title="Nos champions"/></a>
					</article>
					<article>
						<h3>Longbow</h3>
						<a href="" title="Une séance type de tir au longbow"><img src="photos/longbow/seance-type.jpg" alt="Une séance type de tir au longbow" title="Une séance type de tir au longbow"/></a>
						<a href="" title="Une séance en forêt"><img src="photos/longbow/seance-forêt.jpg" alt="Une séance en forêt" title="Une séance en forêt"/></a>
						<a href="" title="Une séance à la campagne"><img src="photos/longbow/tir-campagne.jpg" alt="Une séance à la campagne" title="Une séance à la campagne"/></a>
						<a href="" title="Un longbow"><img src="photos/longbow/longbow.jpg" alt="Un longbow" title="Un longbow"/></a>
						<a href="" title="Un membre tirant au longbow"><img src="photos/longbow/tir-longbow.jpg" alt="Un membre tirant au longbow" title="Un membre tirant au longbow"/></a>
						<a href="" title="Participation de l'équipe à une compétition"><img src="photos/longbow/competition.jpg" alt="Participation de l'équipe à une compétition" title="Participation de l'équipe à une compétition"/></a>
					</article>
				</section>
			</div>
		</main>
			
		<footer>
				<?php include 'include/footer.php'; ?>
		</footer>
	</body>
</html>
