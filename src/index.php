<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8">
		<title>Accueil | Association Robin des bois</title>
		<link rel="icon" type="image/jpg" href="images/favicon.jpg" />
		<link rel="stylesheet" href="style/general.css" type="text/css" />
		<link rel="stylesheet" href="style/index.css" type="text/css" media="screen" />
	</head>
	<body>
		<header>
				<?php include 'include/header.php'; ?>
		</header>

		<main>
			<div class="social">
				<?php include 'include/social.php'; ?>
			</div>
				
			<div class="aside">
				<?php include 'include/aside.php'; ?>
			</div>
				
			<div class="principal">
				<section>
					<h2>Les activités</h2>
					<p><hr /></p>
					<img src="images/longbow.png"/>
					<article>
						<h3>Le tir à l'arc classique</h3>
						<p>Le <em>tir à l'ar classique</em> est l'activité principale de l'association clermontoise de tir à l'arc <strong>Robin des bois</strong>. Lors des séances vous... <a href="activite_tir_classique.php" title="Plus d'information sur les activités de tir à l'arc classique">Lire la suite</a></p>
					</article>
					
					<article>
						<h3>Le tir à l'arc à poulie</h3>
						<p>La deuxième activité proposée est celle du <em>tir à l'arc à poulie<em>, arc plus puissant grâce... <a href="activite_tir_poulie.php" title="Plus d'information sur les activités de tir à l'arc à poulie">Lire la suite</a></p>
					</article>
					
					<article>
						<h3>Le tir à l'arc au longbow</h3>
						<p>Enfin, la dernière activité que nous vous présentons est celle du <em>longbow</em>, un arc d'une pièce très puissant utilisé... <a href="activite_longbow.php" title="Plus d'information sur les activités au longbow">Lire la suite</a></p>
					</article>
				</section>
				
				<section>
					<p><br /><hr /><hr /><br /></p>
						<h2>Nos membres</h2>
						<p><hr /></p>
					<article>
						<img src="images/tir-arc-longbow.jpeg" alt="Tir à l'arc au longbow" title="Tir à l'arc au longbow"/>
						<p>L'association est ouverte à toute personne à partir de 6 ans, et sans... <a href="association.php#membres" title="Plus d'information sur nos membres">Lire la suite</a></p>
					</article>
				</section>
				
				<section>
					<p><br /><hr /><hr /><br /></p>
						<h2>Nos tarifs</h2>
						<p><hr /></p>
					<article>
						<p>Nos tarifs évoluent de <em>1 à 4€ la séance</em>, et de </strong>40 à 85€ à l'année</strong>, en fonc... <a href="tarifs.php">Lire la suite</a></p>
					</article>
				</section>
				
				<section>
					<p><br /><hr /><hr /><br /></p>
						<h2>Le planning 2018-2019</h2>
						<p><hr /></p>
					<article>
						<p>Le planning dépend de l'activité choisie et du niveau d'expérience de celle-ci. <a href="planning.php">Voir le planning</a></p>
					</article>
				</section>
			</div>
		</main>
			
		<footer>
				<?php include 'include/footer.php'; ?>
		</footer>
	</body>
</html>
