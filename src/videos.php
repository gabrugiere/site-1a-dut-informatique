<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8">
		<title>Nos vidéos | Association Robin des bois</title>
		<link rel="icon" type="image/jpg" href="images/favicon.jpg" />
		<link rel="stylesheet" href="style/general.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="style/videos.css" type="text/css" media="screen" />
	</head>
	<body>
		<header>
				<?php include 'include/header.php'; ?>
		</header>

		<main>
			<div class="social">
				<?php include 'include/social.php'; ?>
			</div>
				
			<div class="aside">
				<?php include 'include/aside.php'; ?>
			</div>
			
			<div class="principal">
				<section>
					<h2>Nos vidéos</h2>
					<p><hr /></p>
					<article>
						<p>Les bases du tir à l'arc traditionnel. Une vidéo pour parler des bases du tir, comment bien commencer à tirer à l'arc.</p>
						<video controls preload>
							<source src="videos/Od'arc _ Les bases du tir à l'arc traditionnel.ogv"></source>
							<source src="videos/Od'arc _ Les bases du tir à l'arc traditionnel.mp4"></source>
							<source src="videos/Od'arc _ Les bases du tir à l'arc traditionnel.webm"></source>
						</video>
					</article>
				</section>
			</div>
		</main>
			
		<footer>
				<?php include 'include/footer.php'; ?>
		</footer>
	</body>
</html>
