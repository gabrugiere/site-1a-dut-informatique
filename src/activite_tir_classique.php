<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8">
		<title>Activité arc classique | Association Robin des bois</title>
		<link rel="icon" type="image/jpg" href="images/favicon.jpg" />
		<link rel="stylesheet" href="style/general.css" type="text/css" media="screen" />
	</head>
	<body>
		<header>
				<?php include 'include/header.php'; ?>
		</header>

		<main>
			<div class="social">
				<?php include 'include/social.php'; ?>
			</div>
				
			<div class="aside">
				<?php include 'include/aside.php'; ?>
			</div>
				
			<div class="principal">
				<section>
					<h2>Le tir à l'arc classique</h2>
					<p><hr /></p>
					<article>
						<p>Le tir à l'arc classique est l'activité principale de l'association clermontoise de tir à l'arc Robin des bois. Lors des séances vous utiliserez un arc classique (appelé aussi recurve, ou encore à double courbure) avec lequel vous tirerez sur des cibles à différentes distances, et même de différentes tailles, en fonction de votre niveau et de votre puissance de tir. Certaines séances seront aussi proposées en extérieur, mais alors, attention au vent ;)</p>
					</article>
				</section>
			</div>
		</main>
		
		<footer>
				<?php include 'include/footer.php'; ?>
		</footer>
	</body>
</html>
